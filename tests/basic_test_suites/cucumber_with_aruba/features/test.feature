Feature: Simple tests to showcase that Aruba works

  Background:
    Given a file named "hello-world" with:
    """
    Ahoy!
    """

  Scenario: Functioning executable
    Given an executable named "bin/example-cli" with:
    """
    #!/usr/bin/bash
    echo "Hi!"
    """
    When I successfully run `bin/example-cli`
    Then the output should contain:
    """
    Hi!
    """

  Scenario: Using background step definition
    Given an executable named "bin/example-cli-2" with:
    """
    #!/usr/bin/bash
    cat "./hello-world"
    """
    When I successfully run `bin/example-cli-2`
    Then the output should contain:
    """
    Ahoy!
    """
