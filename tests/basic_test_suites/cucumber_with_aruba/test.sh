#!/bin/sh -eux

PACKAGE=${PACKAGE:-}

[ -n "$PACKAGE" ] || {
  echo "env variable \$PACKAGE not specified, specify what package are we testing." >&2

  exit 1
}

tmp=$(mktemp -d)
cp -r features "$tmp"

pushd "$tmp"
cucumber > test_out.txt

# Provide a way for this test to fail. If something
# is undefined, the exit code of `cucumber` is 0,
# which will not fail the execution of this file.
grep -q "2 scenarios (2 passed)" test_out.txt
grep -q "8 steps (8 passed)" test_out.txt
popd

rm -rf "$tmp"
