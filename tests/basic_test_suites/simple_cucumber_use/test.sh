#!/bin/sh -eux

PACKAGE=${PACKAGE:-}

[ -n "$PACKAGE" ] || {
  echo "env variable \$PACKAGE not specified, specify what package are we testing."

  exit 1
}

echo $(rpm -qa "$PACKAGE")

tmp=$(mktemp -d)
pushd "$tmp"

git clone "https://gitlab.com/jackorp/jekyll-git-authors.git"
pushd jekyll-git-authors
cucumber .
popd

popd
rm -rf "$tmp"
