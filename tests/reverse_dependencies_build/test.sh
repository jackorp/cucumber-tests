#!/bin/sh -eux

BUILD_PACKAGE=${BUILD_PACKAGE:-}
KOJI_TASK_ID=${KOJI_TASK_ID:-}

[ -n "$BUILD_PACKAGE" ] || {
  echo "env variable \$BUILD_PACKAGE not specified, specify what package are we testing." >&2

  exit 1
}

[ -n "$KOJI_TASK_ID" ] || {
  echo "env variable \$KOJI_TASK_ID not specified" >&2
  echo "This variable should be present in TestingFarm" >&2

  exit 1
}

# Inspired by sut_install_commands.sh script available in TestingFarm run
( koji download-build --debuginfo --task-id --arch noarch --arch x86_64 --arch i686 $KOJI_TASK_ID || koji download-task --arch noarch --arch x86_64 --arch i686 $KOJI_TASK_ID ) | grep -E Downloading | cut -d " " -f 3 | tee "rpms-list-$KOJI_TASK_ID"
ls *[^.src].rpm | sed -r "s/(.*)-.*-.*/\1 \0/" | grep -Ev "i686" | awk "{print \$2}" | tee rpms-list

# Prep mock with the packages here, otherwise we'd have to do a bit of
# gymnastics to set the paths correctly in rpms-list.
mock -r fedora-rawhide-x86_64 --verbose -i $(cat rpms-list)

tmp=$(mktemp -d)
pushd $tmp

fedpkg clone -a "rpms/$BUILD_PACKAGE"
# Previous clone will create a directory named after $BUILD_PACKAGE
# if it doesn't exist, it failed, so checking path exists seems pointless
pushd "$BUILD_PACKAGE"
srpm_path=$(fedpkg --release rawhide srpm | grep Wrote | cut -d':' -f2 | tr -d ' ')
# After fedpkg srpm we expect only a single package.
mock -r fedora-rawhide-x86_64 --verbose --no-clean "$srpm_path"
# No other checks on reverse built artifacts should be needed.
# install checks should be in other tests as it's not a focus of this one.
popd

popd
rm -rf "$tmp"
