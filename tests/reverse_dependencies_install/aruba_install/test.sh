#!/bin/sh -eux

PACKAGE=${PACKAGE:-}

[ -n "$PACKAGE" ] || {
  echo "env variable \$PACKAGE not specified, specify what package are we testing."

  exit 1
}

present=$(rpm -qa $PACKAGE)

# We assume dnf install privileges
dnf install -y rubygem-aruba

after_install=$(rpm -qa $PACKAGE)

[ "$present" = "$after_install" ] || {
  echo "Mismatch of $PACKAGE NVR after installing cucumber."
  echo "BEFORE --> $present"
  echo "AFTER  --> $after_install"
  exit 1
}
